const routes = [
  {
    path: '/dashboard/delivery-payments/:id',
    component: () => import('layouts/Dashboard.vue'),
    meta: {
      needAuth: true
    },
    children: [
      {
        path: '',
        component: () => import('pages/dashboard/delivery-payments/view.vue')
      }
    ]
  },
  {
    path: '/dashboard/delivery-payments',
    component: () => import('layouts/Dashboard.vue'),
    meta: {
      needAuth: true
    },
    children: [
      {
        path: '',
        component: () => import('pages/dashboard/delivery-payments/index.vue')
      }
    ]
  },
  {
    path: '/dashboard/share-credits',
    component: () => import('layouts/Dashboard.vue'),
    meta: {
      needAuth: true
    },
    children: [
      {
        path: '',
        component: () => import('pages/dashboard/share-credits/index.vue')
      }
    ]
  },
  {
    path: '/dashboard/share-credits/:id',
    component: () => import('layouts/Dashboard.vue'),
    meta: {
      needAuth: true
    },
    children: [
      {
        path: '',
        component: () => import('pages/dashboard/share-credits/edit.vue')
      }
    ]
  },
  {
    path: '/payment/success',
    component: () => import('layouts/MyLayout.vue'),
    meta: {
      needAuth: true
    },
    children: [
      {
        path: '',
        component: () => import('pages/payments/paypal.vue')
      }
    ]
  },
  {
    path: '/dashboard/home-images',
    component: () => import('layouts/Dashboard.vue'),
    meta: {
      needAuth: true
    },
    children: [
      {
        path: '',
        component: () => import('pages/dashboard/home-images/index.vue')
      }
    ]
  },
  {
    path: '/dashboard/home-images/create',
    component: () => import('layouts/Dashboard.vue'),
    meta: {
      needAuth: true
    },
    children: [
      {
        path: '',
        component: () => import('pages/dashboard/home-images/add.vue')
      }
    ]
  },
  {
    path: '/dashboard/home-images/:id',
    component: () => import('layouts/Dashboard.vue'),
    meta: {
      needAuth: true
    },
    children: [
      {
        path: '',
        component: () => import('pages/dashboard/home-images/edit.vue')
      }
    ]
  },
  {
    path: '/',
    component: () => import('layouts/LandingPage.vue'),
    children: [{ path: '', component: () => import('pages/Index.vue') }]
  },
  {
    path: '/register',
    component: () => import('layouts/LandingPage.vue'),
    children: [{ path: '', component: () => import('pages/register.vue') }]
  },
  {
    path: '/register/activation_code/:token',
    component: () => import('layouts/LandingPage.vue'),
    children: [{ path: '', component: () => import('pages/login.vue') }]
  },
  {
    path: '/password/reset/:token',
    component: () => import('layouts/LandingPage.vue'),
    children: [{ path: '', component: () => import('pages/new-password.vue') }]
  },
  {
    path: '/catalogs',
    component: () => import('layouts/MyLayout.vue'),
    children: [
      { path: '', component: () => import('pages/catalogs/index.vue') }
    ]
  },
  {
    path: '/dashboard/catalogs',
    component: () => import('layouts/Dashboard.vue'),
    meta: {
      needAuth: true
    },
    children: [
      {
        path: '',
        component: () => import('pages/dashboard/catalogs/index.vue')
      }
    ]
  },
  {
    path: '/dashboard/catalogs/create',
    component: () => import('layouts/Dashboard.vue'),
    meta: {
      needAuth: true
    },
    children: [
      { path: '', component: () => import('pages/dashboard/catalogs/add.vue') }
    ]
  },
  {
    path: '/dashboard/catalogs/:id',
    component: () => import('layouts/Dashboard.vue'),
    meta: {
      needAuth: true
    },
    children: [
      { path: '', component: () => import('pages/dashboard/catalogs/edit.vue') }
    ]
  },
  {
    path: '/catalog/:catId/:category',
    component: () => import('layouts/MyLayout.vue'),
    children: [
      { path: '', component: () => import('pages/category/products.vue') }
    ]
  },
  {
    path: '/catalog/:catId/:category/:subCatId/:subCatName',
    component: () => import('layouts/MyLayout.vue'),
    children: [
      {
        path: '',
        component: () => import('pages/category/subcategory/products.vue')
      }
    ]
  },
  {
    path:
      '/catalog/:catId/:category/:subCatId/:subCatName/:furtherCatId/:furtherCat',
    component: () => import('layouts/MyLayout.vue'),
    children: [
      {
        path: '',
        component: () =>
          import('pages/category/subcategory/furthercategory/products.vue')
      }
    ]
  },
  {
    path: '/login',
    component: () => import('layouts/LandingPage.vue'),
    children: [{ path: '', component: () => import('pages/login.vue') }]
  },
  {
    path: '/password/reset',
    component: () => import('layouts/LandingPage.vue'),
    children: [
      { path: '', component: () => import('pages/password-reset.vue') }
    ]
  },
  {
    path: '/products/:category/:id/:product',
    component: () => import('layouts/MyLayout.vue'),
    children: [{ path: '', component: () => import('pages/products/view.vue') }]
  },
  {
    path: '/cart',
    component: () => import('layouts/MyLayout.vue'),
    children: [{ path: '', component: () => import('pages/cart/view.vue') }]
  },
  {
    path: '/checkout',
    component: () => import('layouts/MyLayout.vue'),
    children: [
      { path: '', component: () => import('pages/checkout/index.vue') }
    ]
  },
  {
    path: '/dashboard',
    meta: {
      needAuth: true
    },
    component: () => import('layouts/Dashboard.vue'),
    children: [
      { path: '', component: () => import('pages/dashboard/index.vue') }
    ]
  },
  {
    path: '/dashboard/profile',
    meta: {
      needAuth: true
    },
    component: () => import('layouts/Dashboard.vue'),
    children: [
      { path: '', component: () => import('pages/dashboard/profile.vue') }
    ]
  },
  {
    path: '/dashboard/users',
    meta: {
      needAuth: true
    },
    component: () => import('layouts/Dashboard.vue'),
    children: [
      { path: '', component: () => import('pages/dashboard/users/index.vue') }
    ]
  },
  {
    path: '/dashboard/users/:id',
    meta: {
      needAuth: true
    },
    component: () => import('layouts/Dashboard.vue'),
    children: [
      { path: '', component: () => import('pages/dashboard/users/edit.vue') }
    ]
  },
  {
    path: '/dashboard/users/:id/change-password',
    meta: {
      needAuth: true
    },
    component: () => import('layouts/Dashboard.vue'),
    children: [
      {
        path: '',
        component: () => import('pages/dashboard/users/password.vue')
      }
    ]
  },
  {
    path: '/dashboard/roles',
    meta: {
      needAuth: true
    },
    component: () => import('layouts/Dashboard.vue'),
    children: [
      { path: '', component: () => import('pages/dashboard/roles/index.vue') }
    ]
  },
  {
    path: '/dashboard/role/create',
    meta: {
      needAuth: true
    },
    component: () => import('layouts/Dashboard.vue'),
    children: [
      { path: '', component: () => import('pages/dashboard/roles/add.vue') }
    ]
  },
  {
    path: '/dashboard/roles/:id',
    meta: {
      needAuth: true
    },
    component: () => import('layouts/Dashboard.vue'),
    children: [
      { path: '', component: () => import('pages/dashboard/roles/edit.vue') }
    ]
  },
  {
    path: '/dashboard/categories',
    meta: {
      needAuth: true
    },
    component: () => import('layouts/Dashboard.vue'),
    children: [
      {
        path: '',
        component: () => import('pages/dashboard/categories/index.vue')
      }
    ]
  },
  {
    path: '/dashboard/categories/create',
    meta: {
      needAuth: true
    },
    component: () => import('layouts/Dashboard.vue'),
    children: [
      {
        path: '',
        component: () => import('pages/dashboard/categories/add.vue')
      }
    ]
  },
  {
    path: '/dashboard/categories/:id',
    meta: {
      needAuth: true
    },
    component: () => import('layouts/Dashboard.vue'),
    children: [
      {
        path: '',
        component: () => import('pages/dashboard/categories/edit.vue')
      }
    ]
  },
  {
    path: '/dashboard/access-rights',
    meta: {
      needAuth: true
    },
    component: () => import('layouts/Dashboard.vue'),
    children: [
      {
        path: '',
        component: () => import('pages/dashboard/access-rights/index.vue')
      }
    ]
  },
  {
    path: '/dashboard/access-rights/create',
    meta: {
      needAuth: true
    },
    component: () => import('layouts/Dashboard.vue'),
    children: [
      {
        path: '',
        component: () => import('pages/dashboard/access-rights/add.vue')
      }
    ]
  },
  {
    path: '/dashboard/access-rights/:id',
    meta: {
      needAuth: true
    },
    component: () => import('layouts/Dashboard.vue'),
    children: [
      {
        path: '',
        component: () => import('pages/dashboard/access-rights/edit.vue')
      }
    ]
  },
  {
    path: '/dashboard/stores',
    meta: {
      needAuth: true
    },
    component: () => import('layouts/Dashboard.vue'),
    children: [
      {
        path: '',
        component: () => import('pages/dashboard/branches/index.vue')
      }
    ]
  },
  {
    path: '/dashboard/stores/create',
    meta: {
      needAuth: true
    },
    component: () => import('layouts/Dashboard.vue'),
    children: [
      { path: '', component: () => import('pages/dashboard/branches/add.vue') }
    ]
  },
  {
    path: '/dashboard/stores/:id',
    meta: {
      needAuth: true
    },
    component: () => import('layouts/Dashboard.vue'),
    children: [
      { path: '', component: () => import('pages/dashboard/branches/edit.vue') }
    ]
  },
  {
    path: '/dashboard/products',
    meta: {
      needAuth: true
    },
    component: () => import('layouts/Dashboard.vue'),
    children: [
      {
        path: '',
        component: () => import('pages/dashboard/products/index.vue')
      }
    ]
  },
  {
    path: '/dashboard/products/create',
    meta: {
      needAuth: true
    },
    component: () => import('layouts/Dashboard.vue'),
    children: [
      { path: '', component: () => import('pages/dashboard/products/add.vue') }
    ]
  },
  {
    path: '/dashboard/products/:id',
    meta: {
      needAuth: true
    },
    component: () => import('layouts/Dashboard.vue'),
    children: [
      { path: '', component: () => import('pages/dashboard/products/edit.vue') }
    ]
  },
  {
    path: '/dashboard/my-payments',
    meta: {
      needAuth: true
    },
    component: () => import('layouts/Dashboard.vue'),
    children: [
      {
        path: '',
        component: () => import('pages/dashboard/my-payments/index.vue')
      }
    ]
  },
  {
    path: '/dashboard/my-payments/:id',
    meta: {
      needAuth: true
    },
    component: () => import('layouts/Dashboard.vue'),
    children: [
      {
        path: '',
        component: () => import('pages/dashboard/my-payments/view.vue')
      }
    ]
  },
  {
    path: '/dashboard/users-payments',
    meta: {
      needAuth: true
    },
    component: () => import('layouts/Dashboard.vue'),
    children: [
      {
        path: '',
        component: () => import('pages/dashboard/users-payments/index.vue')
      }
    ]
  },

  {
    path: '/dashboard/users-payments/:id',
    meta: {
      needAuth: true
    },
    component: () => import('layouts/Dashboard.vue'),
    children: [
      {
        path: '',
        component: () => import('pages/dashboard/users-payments/view.vue')
      }
    ]
  },
  {
    path: '/dashboard/delivery-price',
    meta: {
      needAuth: true
    },
    component: () => import('layouts/Dashboard.vue'),
    children: [
      {
        path: '',
        component: () => import('pages/dashboard/delivery-price/index.vue')
      }
    ]
  },
  {
    path: '/dashboard/delivery-price/create',
    meta: {
      needAuth: true
    },
    component: () => import('layouts/Dashboard.vue'),
    children: [
      {
        path: '',
        component: () => import('pages/dashboard/delivery-price/add.vue')
      }
    ]
  },
  {
    path: '/dashboard/delivery-price/:id',
    meta: {
      needAuth: true
    },
    component: () => import('layouts/Dashboard.vue'),
    children: [
      {
        path: '',
        component: () => import('pages/dashboard/delivery-price/edit.vue')
      }
    ]
  },
  {
    path: '/dashboard/purchased-invoices',
    meta: {
      needAuth: true
    },
    component: () => import('layouts/Dashboard.vue'),
    children: [
      {
        path: '',
        component: () => import('pages/dashboard/purchased-invoices/index.vue')
      }
    ]
  },
  {
    path: '/dashboard/purchased-invoices/create',
    meta: {
      needAuth: true
    },
    component: () => import('layouts/Dashboard.vue'),
    children: [
      {
        path: '',
        component: () => import('pages/dashboard/purchased-invoices/add.vue')
      }
    ]
  },
  {
    path: '/dashboard/delivery-received',
    meta: {
      needAuth: true
    },
    component: () => import('layouts/Dashboard.vue'),
    children: [
      {
        path: '',
        component: () => import('pages/dashboard/delivery-received/index.vue')
      }
    ]
  },
  {
    path: '/dashboard/delivery-received/create',
    meta: {
      needAuth: true
    },
    component: () => import('layouts/Dashboard.vue'),
    children: [
      {
        path: '',
        component: () => import('pages/dashboard/delivery-received/add.vue')
      }
    ]
  },
  {
    path: '/dashboard/delivery-received/:id',
    meta: {
      needAuth: true
    },
    component: () => import('layouts/Dashboard.vue'),
    children: [
      {
        path: '',
        component: () => import('pages/dashboard/delivery-received/edit.vue')
      }
    ]
  },
  {
    path: '/dashboard/store-payment-requests',
    meta: {
      needAuth: true
    },
    component: () => import('layouts/Dashboard.vue'),
    children: [
      {
        path: '',
        component: () =>
          import('pages/dashboard/store-payment-requests/index.vue')
      }
    ]
  },
  {
    path: '/dashboard/store-payment-requests/create',
    meta: {
      needAuth: true
    },
    component: () => import('layouts/Dashboard.vue'),
    children: [
      {
        path: '',
        component: () =>
          import('pages/dashboard/store-payment-requests/add.vue')
      }
    ]
  },
  {
    path: '/dashboard/store-payment-requests/:id',
    meta: {
      needAuth: true
    },
    component: () => import('layouts/Dashboard.vue'),
    children: [
      {
        path: '',
        component: () =>
          import('pages/dashboard/store-payment-requests/view.vue')
      }
    ]
  },
  {
    path: '/dashboard/payment-requests',
    meta: {
      needAuth: true
    },
    component: () => import('layouts/Dashboard.vue'),
    children: [
      {
        path: '',
        component: () => import('pages/dashboard/payment-requests/index.vue')
      }
    ]
  },
  {
    path: '/dashboard/payment-requests/create',
    meta: {
      needAuth: true
    },
    component: () => import('layouts/Dashboard.vue'),
    children: [
      {
        path: '',
        component: () => import('pages/dashboard/payment-requests/add.vue')
      }
    ]
  },
  {
    path: '/dashboard/payment-requests/:id',
    meta: {
      needAuth: true
    },
    component: () => import('layouts/Dashboard.vue'),
    children: [
      {
        path: '',
        component: () => import('pages/dashboard/payment-requests/edit.vue')
      }
    ]
  },
  {
    path: '/dashboard/feedback/:invoice/:id',
    meta: {
      needAuth: true
    },
    component: () => import('layouts/Dashboard.vue'),
    children: [
      {
        path: '',
        component: () => import('pages/products/feedback.vue')
      }
    ]
  }
]

// Always leave this as last one
if (process.env.MODE !== 'ssr') {
  routes.push({
    path: '*',
    component: () => import('pages/Error404.vue')
  })
}

export default routes
