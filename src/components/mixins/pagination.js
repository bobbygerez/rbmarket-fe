export default {
  data() {
    return {
      debouncedFunction: "",
      loading: false,
      filter: "",
      options: [5, 10, 15, 20],
      serverPagination: {
        page: 1,
        rowsNumber: 10,
        rowsPerPage: 10 // specifying this determines pagination is server-side
      },
      serverData: []
    };
  },
  methods: {
    request(props) {
      this.debouncedFunction(props);
    }
  }
};
