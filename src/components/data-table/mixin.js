export default {
  props: {
    viewPayment: {
      type: [Boolean],
      default: false
    },
    payments: {
      type: [Boolean],
      default: false
    },
    products: {
      type: [Boolean],
      default: false
    },
    restoreData: {
      type: [Boolean],
      default: false
    },
    entity: {
      type: [String],
      default: null
    },
    // the table data
    searchPlaceholder: {
      type: [String],
      default: null
    },
    data: {
      type: [Array, Object],
      default: null
    },
    // column definition
    columns: {
      type: [Array, Object],
      default: null
    },
    search: {
      Boolean,
      default: true
    },
    grid: {
      Boolean,
      default: false
    },
    // current pagination settings of the table
    pagination: {
      type: Object,
      default: () => {
        return {
          rowsPerPage: 7,
          page: 1
        };
      }
    },
    rowOptions: {
      type: Array,
      default: () => [3, 5, 7, 10, 15, 25, 50, 0]
    },

    // the filter model of your table
    searchField: {
      type: String,
      default: () => ""
    },

    title: {
      type: String,
      default: () => ""
    },
    loading: {
      type: Boolean,
      default: false
    }
  }
};
