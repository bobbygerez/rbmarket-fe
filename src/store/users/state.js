export default {
  facebook: {
    FB: {},
    model: {},
    appId: "479157435965455"
  },
  dialogChangePassword: false,
  editBrgys: [],
  editCities: [],
  editProvinces: [],
  roles: [],
  editUser: {
    fullname: ""
  },
  users: {
    current_page: 1,
    data: []
  },
  user: {
    fullname: "Hello, login",
    firstname: "",
    lastname: "",
    mobile: "",
    notes: "",
    address: {
      province_id: "",
      city_id: "",
      brgy_id: ""
    }
  },
  delivery: {
    firstname: "",
    lastname: "",
    mobile: "",
    notes: "",
    address: {
      province_id: "",
      city_id: "",
      brgy_id: ""
    }
  },
  token: "",
  page: 1,
  perPage: 30
};
