export const setStoreInvoice = ({ commit }, payload) => {
  commit('setStoreInvoice', payload)
}

export const setInvoiceItems = ({ commit }, payload) => {
  commit('setInvoiceItems', payload)
}
