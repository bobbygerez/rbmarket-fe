export const setStoreInvoice = (state, payload) => {
  state.storeInvoice = payload
}

export const setInvoiceItems = (state, payload) => {
  state.invoiceItems = payload
}
