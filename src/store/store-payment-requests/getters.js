export const selectedStore = state => state.selectedStore;

export const selectedDeliveryReceived = state => state.selectedDeliveryReceived;

export const products = state => state.products;

export const grandTotal = state => state.grandTotal;

export const remarks = state => state.remarks;

export const address = state => state.address;

export const isPaid = state => state.isPaid;

export const paymentImages = state => state.paymentImages;
