export const setSelectedStore = ({ commit }, payload) => {
  commit("setSelectedStore", payload);
};

export const setSelectedDeliveryReceived = ({ commit }, payload) => {
  commit("setSelectedDeliveryReceived", payload);
};

export const setProducts = ({ commit }, payload) => {
  commit("setProducts", payload);
};

export const setGrandTotal = ({ commit }, payload) => {
  commit("setGrandTotal", payload);
};

export const setRemarks = ({ commit }, payload) => {
  commit("setRemarks", payload);
};

export const setAddress = ({ commit }, payload) => {
  commit("setAddress", payload);
};

export const setIsPaid = ({ commit }, payload) => {
  commit("setIsPaid", payload);
};

export const setPaymentImages = ({ commit }, payload) => {
  commit("setPaymentImages", payload);
};
