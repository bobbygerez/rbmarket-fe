export const setSelectedStore = (state, payload) => {
  state.selectedStore = payload;
};

export const setSelectedDeliveryReceived = (state, payload) => {
  state.selectedDeliveryReceived = payload;
};

export const setProducts = (state, payload) => {
  state.products = payload;
};

export const setGrandTotal = (state, payload) => {
  state.grandTotal = payload;
};
export const setRemarks = (state, payload) => {
  state.remarks = payload;
};

export const setAddress = (state, payload) => {
  state.address = payload;
};

export const setIsPaid = (state, payload) => {
  state.isPaid = payload;
};

export const setPaymentImages = (state, payload) => {
  state.paymentImages = payload;
};
