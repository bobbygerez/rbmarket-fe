export default {
  selectedStore: null,
  selectedDeliveryReceived: null,
  products: [],
  grandTotal: 0,
  remarks: "",
  address: {
    complete_address: ""
  },
  paymentImages: []
};
