export default {
  currentPage: 1,
  deliveryPrice: 0,
  leftDrawer: true,
  breadCrumbs: [],
  cart: [],
  qty: 1,
  totalCart: 0,
  cartLength: "",
  provinces: [],
  cities: [],
  delCities: [],
  delBrgys: [],
  brgys: [],
  searchStringProduct: "",
  categories: [],
  allCategories: []
};
