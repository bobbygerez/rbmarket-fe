export const setLeftDrawer = ({ commit }, payload) => {
  commit("setLeftDrawer", payload);
};

export const setSearchStringProduct = ({ commit }, payload) => {
  commit("setSearchStringProduct", payload);
};

export const setBreadCrumbs = ({ commit }, payload) => {
  commit("setBreadCrumbs", payload);
};

export const setCart = ({ commit }, payload) => {
  commit("setCart", payload);
};

export const setDefaultCart = ({ commit }, payload) => {
  commit("setDefaultCart", payload);
};

export const setQty = ({ commit }, payload) => {
  commit("setQty", payload);
};

export const setTotalCart = ({ commit }, payload) => {
  commit("setTotalCart", payload);
};

export const setCartLength = ({ commit }, payload) => {
  commit("setCartLength", payload);
};

export const deleteCartRow = ({ commit }, payload) => {
  commit("deleteCartRow", payload);
};

export const setProvinces = ({ commit }, payload) => {
  commit("setProvinces", payload);
};

export const setDelCities = ({ commit }, payload) => {
  commit("setDelCities", payload);
};

export const setCities = ({ commit }, payload) => {
  commit("setCities", payload);
};

export const setDelBrgys = ({ commit }, payload) => {
  commit("setDelBrgys", payload);
};

export const setBrgys = ({ commit }, payload) => {
  commit("setBrgys", payload);
};

export const setDeliveryPrice = ({ commit }, payload) => {
  commit("setDeliveryPrice", payload);
};

export const setCategories = ({ commit }, payload) => {
  commit("setCategories", payload);
};

export const setAllCategories = ({ commit }, payload) => {
  commit("setAllCategories", payload);
};

export const setAddQty = ({ commit }, payload) => {
  commit("setAddQty", payload);
};
export const setRemoveQty = ({ commit }, payload) => {
  commit("setRemoveQty", payload);
};

export const setCurrentPage = ({ commit }, payload) => {
  commit("setCurrentPage", payload);
};
