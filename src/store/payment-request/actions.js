export const setPaymentRequest = ({ commit }, payload) => {
  commit("setPaymentRequest", payload);
};

export const setGrandTotal = ({ commit }, payload) => {
  commit("setGrandTotal", payload);
};

export const setRemarks = ({ commit }, payload) => {
  commit("setRemarks", payload);
};

export const setIsPaid = ({ commit }, payload) => {
  commit("setIsPaid", payload);
};

export const setAddress = ({ commit }, payload) => {
  commit("setAddress", payload);
};

export const setProducts = ({ commit }, payload) => {
  commit("setProducts", payload);
};
