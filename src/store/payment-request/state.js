export default {
  paymentRequest: [],
  grandTotal: 0,
  remarks: "",
  isPaid: false,
  address: {
    complete_address: ""
  },
  products: []
};
