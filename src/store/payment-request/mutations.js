export const setPaymentRequest = (state, payload) => {
  state.paymentRequest = payload;
};

export const setGrandTotal = (state, payload) => {
  state.grandTotal = payload;
};

export const setRemarks = (state, payload) => {
  state.remarks = payload;
};

export const setIsPaid = (state, payload) => {
  state.isPaid = payload;
};

export const setAddress = (state, payload) => {
  state.address = payload;
};

export const setProducts = (state, payload) => {
  state.products = payload;
};
