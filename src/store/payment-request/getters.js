export const paymentRequest = state => state.paymentRequest;

export const grandTotal = state => state.grandTotal;

export const remarks = state => state.remarks;

export const isPaid = state => state.isPaid;

export const address = state => state.address;

export const products = state => state.products;
