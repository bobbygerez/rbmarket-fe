export const setMobile = ({ commit }, payload) => {
  commit('setMobile', payload)
}

export const setPaymentOptionn = ({ commit }, payload) => {
  commit('setPaymentOptionn', payload)
}

export const setMarkersPosition = ({ commit }, payload) => {
  commit('setMarkersPosition', payload)
}

export const setAddress = ({ commit }, payload) => {
  commit('setAddress', payload)
}
