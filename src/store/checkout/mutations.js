export const setMobile = (state, payload) => {
  state.mobile = payload
}

export const setPaymentOptionn = (state, payload) => {
  state.paymentOptionn = payload
}

export const setMarkersPosition = (state, payload) => {
  state.markersPosition = payload
}

export const setAddress = (state, payload) => {
  state.address = payload
}
