export default {
  mobile: "",
  paymentOptionn: {
    value: null,
    label: ""
  },
  markersPosition: {
    lat: 10.333333,
    lng: 123.933334
  },
  address: {
    is_default: true,
    province_id: null,
    city_id: null,
    brgy_id: null,
    street_lot_blk: "",
    complete_address: ""
  }
};
