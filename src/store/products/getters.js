export const products = state => state.products;

export const product = state => state.product;

export const mainCategories = state => state.mainCategories;

export const subCategories = state => state.subCategories;

export const moreCategories = state => state.moreCategories;

export const stars = state => state.stars;

export const comments = state => state.comments;

export const rating = state => state.rating;

export const category = state => state.category;
