export const payment = state => state.payment

export const paypalInfo = state => state.paypalInfo

export const cartt = state => state.cartt

export const paymentOptions = state => state.paymentOptions

export const grandTotal = state => state.grandTotal
