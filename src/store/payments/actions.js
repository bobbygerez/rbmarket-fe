export const setPayment = ({ commit }, payload) => {
  commit('setPayment', payload)
}

export const setPaypalInfo = ({ commit }, payload) => {
  commit('setPaypalInfo', payload)
}

export const setCartt = ({ commit }, payload) => {
  commit('setCartt', payload)
}

export const setPaymentOptions = ({ commit }, payload) => {
  commit('setPaymentOptions', payload)
}

export const setGrandTotal = ({ commit }, payload) => {
  commit('setGrandTotal', payload)
}
