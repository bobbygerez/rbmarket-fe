export const setPayment = (state, payload) => {
  state.payment = payload
}

export const setPaypalInfo = (state, payload) => {
  state.paypalInfo = payload
}

export const setCartt = (state, payload) => {
  state.cartt = payload
}

export const setPaymentOptions = (state, payload) => {
  state.paymentOptions = payload
}

export const setGrandTotal = (state, payload) => {
  state.grandTotal = payload
}
