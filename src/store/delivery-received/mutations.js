export const setDeliveryReceived = (state, payload) => {
  state.deliveryReceived = payload
}

export const setStores = (state, payload) => {
  state.stores = payload
}

export const setSelectedStore = (state, payload) => {
  state.selectedStore = payload
}

export const setStoreInvoices = (state, payload) => {
  state.storeInvoices = payload
}

export const setProducts = (state, payload) => {
  state.products = payload
}
