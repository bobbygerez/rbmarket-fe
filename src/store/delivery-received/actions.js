export const setDeliveryReceived = ({ commit }, payload) => {
  commit('setDeliveryReceived', payload)
}

export const setStores = ({ commit }, payload) => {
  commit('setStores', payload)
}

export const setSelectedStore = ({ commit }, payload) => {
  commit('setSelectedStore', payload)
}

export const setStoreInvoices = ({ commit }, payload) => {
  commit('setStoreInvoices', payload)
}

export const setProducts = ({ commit }, payload) => {
  commit('setProducts', payload)
}
