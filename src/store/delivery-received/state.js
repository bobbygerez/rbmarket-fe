export default {
  selectedStore: null,
  deliveryReceived: {
    store_invoice_id: null,
    no: null
  },
  stores: [],
  storeInvoices: [],
  products: []
}
