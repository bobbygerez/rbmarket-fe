export const deliveryReceived = state => state.deliveryReceived

export const stores = state => state.stores

export const selectedStore = state => state.selectedStore

export const storeInvoices = state => state.storeInvoices

export const products = state => state.products
